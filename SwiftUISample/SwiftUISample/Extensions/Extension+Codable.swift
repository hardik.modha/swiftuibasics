//
//  Extension+Codable.swift
//  PaginationSample
//
//  Created by mac-00021 on 14/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import Foundation

extension Decodable {
    
    static func decode<A: Decodable>(from data: Data) -> Result<A> {
        do {
            let result =  try JSONDecoder().decode(A.self, from: data)
            return .success(result)
        } catch let error {
            print(error.localizedDescription)
            let apiError = APIError(title: "Something went wrong", message: "getting error while fetching response")
            return .failure(.custom(apiError))
        }
    }
}

extension Encodable {

    var encodedData: JSONType? {
        do {
            let encoded = try JSONEncoder().encode(self)
            let result = try JSONSerialization.jsonObject(with: encoded, options: .mutableContainers) as? JSONType
            return result
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
}
