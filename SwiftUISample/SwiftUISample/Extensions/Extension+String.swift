//
//  Extension+String.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import Foundation

extension String {

    var isValidEmailAddress: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }

    var imageData: Data? {
        do {
            let data = try Data(contentsOf: URL(string: self)!)
            return data
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
        
    }
}
