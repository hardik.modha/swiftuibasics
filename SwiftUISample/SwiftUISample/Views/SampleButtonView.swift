//
//  SampleButtonView.swift
//  SampleSwiftUI
//
//  Created by mac-00017 on 30/06/21.
//

import SwiftUI

struct SampleButtonView: View {

    @State private var showAlert: Bool = false
    var title: String
    var message: String

    var body: some View {
        Button(action: {
            self.showAlert.toggle()

        }, label: {
            Text("Tapped Me")
                .font(.system(size: 15.0, weight: .bold, design: .monospaced))
                .padding()
                .foregroundColor(.white)
                .background(Color.gray)
                .border(Color.black, width: 1.5)
                .shadow(color: .black, radius: 10, x: 1.5, y: 1.5)
                .cornerRadius(4.0)
        })
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(self.title), message: Text(self.message), dismissButton: .default(Text("OK")))
        })
        .navigationBarTitle("Button & Alert Demo", displayMode: .inline)
    }
}

struct SampleButtonView_Previews: PreviewProvider {
    static var previews: some View {
        SampleButtonView(title: "title", message: "message")
    }
}
