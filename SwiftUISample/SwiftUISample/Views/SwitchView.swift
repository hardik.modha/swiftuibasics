//
//  SwitchView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 01/07/21.
//

import SwiftUI

struct SwitchView: View {

    @State private var isOn: Bool = false

    var body: some View {
        VStack {
            Toggle("Switch", isOn: $isOn)
            Text("Switch is \(isOn ? "On" : "Off")")
                .foregroundColor(isOn ? .green : .red)
        }
        .padding()
        .navigationBarTitle("Switch Demo", displayMode: .inline)
    }
}

struct SwitchView_Previews: PreviewProvider {
    static var previews: some View {
        SwitchView()
    }
}
