//
//  DatePickerView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 01/07/21.
//

import SwiftUI

struct DatePickerView: View {

    @State private var selectedDate: Date = Date()

    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()


    var body: some View {
        VStack {
            DatePicker("Select Date", selection: $selectedDate, displayedComponents: .date)
            Text("Selected Date: \(selectedDate, formatter: dateFormatter)")
        }
        .padding()
        .navigationBarTitle("DatePicker Demo", displayMode: .inline)

    }

}

struct DatePickerView_Previews: PreviewProvider {
    static var previews: some View {
        DatePickerView()
    }
}
