//
//  ListViewDemo.swift
//  SwiftUISample
//
//  Created by mac-00017 on 02/07/21.
//

import SwiftUI

struct ListViewDemo: View {
    @State private var cities: [City] = JSONHelper.getCities()
    var body: some View {
        List {
            ForEach(cities, id: \.id) { (city) in
                Section(header: Text(city.name)) {
                    ForEach(city.landmarks, id: \.id) { (landmark) in
                        NavigationLink(
                            destination: CityDetailView(landMark: landmark),
                            label: {
                                HStack(spacing: 16) {
                                    Image(landmark.photo)
                                        .resizable()
                                        .frame(width: 50, height: 50, alignment: .center)
                                    Text(landmark.name)
                                }
                            })
                    }
                }
            }

        }
        .navigationBarTitle("Landmarks", displayMode: .inline)
    }



}

struct ListViewDemo_Previews: PreviewProvider {
    static var previews: some View {
        ListViewDemo()
    }
}
