//
//  UserDetailView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 06/07/21.
//

import SwiftUI
import MapKit


struct UserDetailView: View {

    var user: User

    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: user.address?.geo?.latitude ?? 0.0, longitude: user.address?.geo?.logitutude ?? 0.0), span: MKCoordinateSpan())

    var body: some View {
        Map(coordinateRegion: $region)
            .frame(maxWidth: .infinity, maxHeight: .infinity)

    }
}

struct UserDetailView_Previews: PreviewProvider {
    static var previews: some View {
        UserDetailView(user: User(id: nil, name: nil, username: nil, email: nil, address: nil, phone: nil, website: nil, company: nil))
    }
}
