//
//  TaskListView.swift
//  SampleSwiftUI
//
//  Created by mac-00017 on 30/06/21.
//

enum TaskList: Int, CustomStringConvertible, CaseIterable {
    case Text = 0
    case TextField
    case Button
    case Stepper
    case Slider
    case Switch
    case ColorPicker
    case DatePikcer
    case ActivityIndicator
    case Login
    case Tab
    case List
    case UserList
    case Map
    case Stack

    var id: Int {
        return self.rawValue
    }

    var description: String {
        switch self {
        case .Text:
            return "TextSampleDemo"
        case .Button:
            return "Button & Alert Demo"
        case .Stepper:
            return "StepperSampleDemo"
        case .List:
            return "ListSampleDemo"
        case .TextField:
            return "TexFieldSampleDemo"
        case .ColorPicker:
            return "ColorPikcerSampleDemo"
        case .DatePikcer:
            return "DatePikcerSampleDemo"
        case .Stack:
            return "StackSampleDemo"
        case .Slider:
            return "SliderSampleDemo"
        case .Switch:
            return "SwitchSampleDemo"
        case .Tab:
            return "TabViewDemo"
        case .ActivityIndicator:
            return "ActivityIndicatorDemo"
        case .Login:
            return "LoginDemo"
        case .UserList:
            return "User List"
        case .Map:
            return "Map"
        }
    }
}


import SwiftUI

struct TaskListView: View {
    @State private var taskList: [TaskList] = TaskList.allCases
    var body: some View {
        NavigationView {
            List {
                ForEach(taskList, id: \.self.id) { task in
                    NavigationLink(
                        destination: MediatorView(task: task),
                        label: {
                            Text(task.description)
                        })
                }
            }
            .navigationBarTitle(Text("Task List"), displayMode: .inline)
        }
    }
}

struct TaskListView_Previews: PreviewProvider {
    static var previews: some View {
        TaskListView()
    }
}
