//
//  ResuableTextView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 06/07/21.
//

import SwiftUI

struct ResuableTextView: View {
    var title: String
    var subTitle: String

    var font: Font = .title

    var body: some View {
        VStack(alignment: .leading, spacing: 2, content: {
            if !title.isEmpty {
                Text(title)
                    .font(font)
            }

            if !subTitle.isEmpty {
                Text(subTitle)
            }

        })
    }
}

struct ResuableTextView_Previews: PreviewProvider {
    static var previews: some View {
        ResuableTextView(title: "Hardik", subTitle: "Modha")
    }
}
