//
//  CityDetailView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 02/07/21.
//

import SwiftUI

struct CityDetailView: View {
    var landMark: Landmark

    var body: some View {
        ScrollView(.vertical, showsIndicators: true, content: {
            VStack {
                Image(landMark.photo)
                    .resizable()
                    .frame(height: 200, alignment: .leading)
                    .scaledToFill()
                Divider()
                Text("About")
                    .font(.system(size: 15, weight: .bold, design: .default))
                    .multilineTextAlignment(.leading)
                Text(landMark.landmarkDescription)
                Spacer()

            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        })
        .padding()
        .navigationBarTitle(landMark.name, displayMode: .inline)
    }
}

struct CityDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CityDetailView(landMark: Landmark.landmark)
    }
}
