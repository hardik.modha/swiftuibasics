//
//  UserListView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import SwiftUI

struct UserListView: View {

    @State private var fetchingState: FetchingState = .fetching
    @ObservedObject var userViewModal = UserViewModal()

    var body: some View {
        if fetchingState.isFetching {
            ActivityIndicatorView()
                .onAppear(perform: {
                    self.fetch()
                })
                .navigationBarTitle("Users", displayMode: .inline)
        } else {
            List {
                ForEach(self.userViewModal.user, id: \.id) { (user) in
                    NavigationLink(
                        destination: PostView(user: user),
                        label: {
                            UserView(user: user)
                        })
                }
            }
        }

    }


    func fetch()  {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.userViewModal.fetchUser { isFetched in
                if isFetched {
                    DispatchQueue.main.async {
                        self.fetchingState = .fetched
                    }

                }
            }
        }
    }
}

struct UserListView_Previews: PreviewProvider {
    static var previews: some View {
        UserListView()
    }
}
