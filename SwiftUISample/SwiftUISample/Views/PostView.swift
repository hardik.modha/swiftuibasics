//
//  PostView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 06/07/21.
//

import SwiftUI

struct PostView: View {
    @ObservedObject var userViewModel = UserViewModal()
    @State private var fetchingState: FetchingState = .fetching
    var user: User
    var body: some View {
        if self.fetchingState.isFetching {
            ActivityIndicatorView()
                .onAppear(perform: {
                    self.fetchPosts()
                })
                .navigationBarTitle("Posts", displayMode: .inline)
        } else {
            List {
                ForEach(userViewModel.posts, id: \.id) { (post) in
                    ResuableTextView(title: post.title!, subTitle: post.body!, font: .title)
                }
            }

        }
    }   

    func fetchPosts()  {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            userViewModel.fetchPost() { isFetched in
                if isFetched {
                    DispatchQueue.main.async {
                        self.fetchingState = .fetched
                    }

                }

            }
        }

    }
}

struct PostView_Previews: PreviewProvider {
    static var previews: some View {
        PostView(user: User(id: nil, name: nil, username: nil, email: nil, address: nil, phone: nil, website: nil, company: nil))
    }
}
