//
//  TextFieldView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 01/07/21.
//

import SwiftUI

struct TextFieldView: View {

    @State private var text: String = ""
    @State private var secured: String = ""

    var body: some View {
        VStack(alignment: .center, spacing: 16, content: {
            HStack(spacing: 0) {
                Image("mail")
                    .resizable()
                    .frame(width: 20, height: 20, alignment: .center)
                    .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 10))
                TextField("Placeholder", text: $text)
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                }
            .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
            .border(Color.black, width: 1)
            .cornerRadius(3.0)
        })
        .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
        .navigationBarTitle("TextField Demo", displayMode: .inline)


    }
}

struct TextFieldView_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldView()
    }
}
