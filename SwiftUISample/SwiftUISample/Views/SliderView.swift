//
//  SliderView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 01/07/21.
//

import SwiftUI

struct SliderView: View {

    @State private var sliderValue: Double = 0.0

    var body: some View {
        VStack {
            Slider(value: $sliderValue, in: 0...20)
                .accentColor(sliderValue < 10 ? .blue : .red)
            Text("Current slider value: \(sliderValue, specifier: "%.2f")")
            Text("you are in: \(sliderValue < 10 ? "safe zone" : "danzer zone")")

        }
        .padding()
        .navigationBarTitle("Slider Demo", displayMode: .inline)
    }
}

struct SliderView_Previews: PreviewProvider {
    static var previews: some View {
        SliderView()
    }
}
