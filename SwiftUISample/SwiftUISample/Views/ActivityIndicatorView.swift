//
//  ActivityIndicatorView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import SwiftUI

struct ActivityIndicatorView: View {
    var body: some View {
        ProgressView("Loading")
            .progressViewStyle(CircularProgressViewStyle())
    }
}

struct ActivityIndicatorView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityIndicatorView()
    }
}
