//
//  MediatorView.swift
//  SampleSwiftUI
//
//  Created by mac-00017 on 30/06/21.
//

import SwiftUI

struct MediatorView: View {
    var task: TaskList

    var body: some View {
        switch task {
        case .Text:
             SampleTextView()
                .navigationBarTitle(Text(task.description), displayMode: .inline)
        case .TextField:
             TextFieldView()
        case .Button:
            SampleButtonView(title: "Hello", message: "How are you")
        case .Stepper:
            StepperView()
        case .Slider:
            SliderView()
        case .Switch:
            SwitchView()
        case .ColorPicker:
            ColorPickerView()
        case .Tab:
            TabSampleView()
        case .List:
            ListViewDemo()
        case .DatePikcer,.Stack:
             Text("Comming Soon")
        case .ActivityIndicator:
            ActivityIndicatorView()
        case .Login:
            LoginView()
        case .UserList:
            UserListView()
        case .Map:
            MapView()
        }
    }

}

struct MediatorView_Previews: PreviewProvider {
    static var previews: some View {
        MediatorView(task: .Text)
    }
}
