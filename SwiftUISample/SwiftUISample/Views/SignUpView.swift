//
//  SignUpView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 07/07/21.
//

import SwiftUI

struct SignUpView: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var passwordAgain: String = ""
    var body: some View {
        Form {
            Section(header: Text("Username")) {
                TextField("Username", text: $username)
            }

            Section(header: Text("Password")) {
                SecureField("Password", text: $password)
                SecureField("Password Again", text: $passwordAgain)
            }
        }
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
    }
}
