//
//  MapView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 06/07/21.
//

import SwiftUI
import MapKit

struct MapView: View {

    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 23.0701125, longitude: 72.5152489), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.2))

    var body: some View {
        Map(coordinateRegion: $region)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .navigationBarTitle("Map", displayMode: .inline)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
