//
//  UserView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import SwiftUI
import AlamofireImage
import UIKit

struct UserView: View {

    var user: User

    var body: some View {
        HStack(alignment: .center) {
            if !(user.name?.isEmpty ?? false) || !(user.email?.isEmpty ?? false) {
                ResuableTextView(title: user.name ?? "", subTitle: user.email ?? "", font: .body)
            }
            Spacer()
        }
        .padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))
        .navigationBarTitle("Users", displayMode: .inline)
    }
}

struct UserView_Previews: PreviewProvider {
    static var previews: some View {
        UserView(user: User(id: nil, name: nil, username: nil, email: nil, address: nil, phone: nil, website: nil, company: nil))
    }
}
