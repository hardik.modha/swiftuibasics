//
//  SampleTextView.swift
//  SampleSwiftUI
//
//  Created by mac-00017 on 30/06/21.
//

import SwiftUI

struct SampleTextView: View {
    var body: some View {

        Text("Hello")
            .font(.title)
            .bold()
            .multilineTextAlignment(.center)
            .foregroundColor(.red)
            .shadow(color: .black, radius: 10, x: 1, y: 1)
            .padding()
            .navigationBarTitle("TextSampleDemo", displayMode: .inline)
    }
}

struct SampleTextView_Previews: PreviewProvider {
    static var previews: some View {
        SampleTextView()
    }
}
