//
//  TabSampleView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 02/07/21.
//

import SwiftUI

struct TabSampleView: View {

    @State private var selectedTab: Int = 0

    var body: some View {
        TabView(selection: $selectedTab,
                content: {
            SliderView()
                .tabItem {
                    Label("Slider", image: "")
                }

            SwitchView()
                .tabItem {
                    Label("Switch", image: "")
                }

            SampleButtonView(title: "Hii", message: "Bye")
                .tabItem {
                    Label("Button", image: "")
                }
        })
        .font(.title2)
        .navigationBarTitle("TabDemo", displayMode: .inline)
    }
}

struct TabSampleView_Previews: PreviewProvider {
    static var previews: some View {
        TabSampleView()
    }
}
