//
//  StepperView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 01/07/21.
//

import SwiftUI

struct StepperView: View {
    @State private var value: Int = 0
    var body: some View {
        VStack {
            Text("value: \(value)")
            Stepper("Amount") {
                self.value += 1
            } onDecrement: {
                self.value -= 1
            }
            .padding()

        }
        .padding()
        .navigationBarTitle("Stepper Demo", displayMode: .inline)
    }
}

struct StepperView_Previews: PreviewProvider {
    static var previews: some View {
        StepperView()
    }
}
