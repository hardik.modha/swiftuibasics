//
//  LoginView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import SwiftUI

struct LoginView: View {
    @ObservedObject private var loginViewModel = LoginViewModel()
    var body: some View {
        VStack {
            TextField("Email", text: $loginViewModel.email)
                .keyboardType(.emailAddress)
            SecureField("Password", text: $loginViewModel.password)
                .keyboardType(.default)
            NavigationLink(
                destination: UserListView(),
                isActive: self.$loginViewModel.isNavigate,
                label: {
                    Button("Login") {
                        self.loginViewModel.validateEntries()
                        if loginViewModel.isValid {
                            self.loginViewModel.authenticatedUser()
                        }
                    }
                    .alert(isPresented: $loginViewModel.isShowAlert, content: {
                        Alert(title: Text(loginViewModel.error.title), message: Text(loginViewModel.error.message), dismissButton: .cancel(Text("OK")))
                    })
                })
        }
        .padding()
        .textFieldStyle(RoundedBorderTextFieldStyle())
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
