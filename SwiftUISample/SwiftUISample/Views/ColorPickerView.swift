//
//  ColorPickerView.swift
//  SwiftUISample
//
//  Created by mac-00017 on 01/07/21.
//

import SwiftUI

struct ColorPickerView: View {
    @State private var pickedColor: Color = .red

    var body: some View {
        VStack {
            ColorPicker("Pikced Color", selection: $pickedColor)
                .padding()

        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(pickedColor)
        .navigationBarTitle("Color Picker Demo", displayMode: .inline)
    }
}

struct ColorPickerView_Previews: PreviewProvider {
    static var previews: some View {
        ColorPickerView()
    }
}
