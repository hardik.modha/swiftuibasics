//
//  Post.swift
//  SwiftUISample
//
//  Created by mac-00017 on 06/07/21.
//

import Foundation

struct Post: Codable {
    var userID: Int?
    var id: Int?
    var title: String?
    var body: String?

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}
