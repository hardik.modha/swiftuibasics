//
//  User.swift
//  SwiftUISample
//
//  Created by mac-00017 on 06/07/21.
//

import Foundation
import MapKit
import CoreLocation.CLLocation

struct User: Codable {
    var id: Int?
    var name: String?
    var username: String?
    var email: String?
    var address: Address?
    var phone: String?
    var website: String?
    var company: Company?
}

// MARK: - Address
struct Address: Codable {
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geo: Geo?
}

// MARK: - Geo
struct Geo: Codable {
    var lat: String?
    var lng: String?
}

extension Geo {
    var latitude: CLLocationDegrees {
        if let latitude = self.lat {
            return CLLocationDegrees(latitude) ?? 0.0
        }
        return 0.0
    }

    var logitutude: CLLocationDegrees {
        if let logitude = self.lng {
            return CLLocationDegrees(logitude) ?? 0.0
        }

        return 0.0
    }
}

// MARK: - Company
struct Company: Codable {
    var name: String?
    var catchPhrase: String?
    var bs: String?
}
