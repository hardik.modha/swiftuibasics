//
//  Student.swift
//  SwiftUISample
//
//  Created by mac-00017 on 02/07/21.
//

import Foundation

struct Student {
    var firstName: String
    var lastName: String
    var rollNo: Int
}

extension Student {
    var fullName: String {
        return firstName + " " + lastName
    }

    static var list: [Student] {
        return [Student(firstName: "Hardik", lastName: "Modha", rollNo: 294),
                Student(firstName: "Durgesh", lastName: "Vadher", rollNo: 300),
                Student(firstName: "Durgesh", lastName: "Vadher", rollNo: 035)]
    }
}

