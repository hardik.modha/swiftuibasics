//
//  LoginValidation.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import Foundation



struct APIError {
    var title: String
    var message: String
}

struct LoginValidation {

    func validateInputs(withEmail email: String, andPassword password: String, completion: @escaping ResultHandler<Void>)  {
        guard !email.isEmpty else {
            completion(.failure(.custom(APIError(title: "Email Empty", message: "Email can't be empty."))))
            return
        }

        guard email.isValidEmailAddress else {
            completion(.failure(.custom(APIError(title: "Invalid Email", message: "Enter valid email Id."))))
            return
        }

        guard !password.isEmpty else {
            completion(.failure(.custom(APIError(title: "Passowrd Empty", message: "Password can't be empty"))))
            return
        }

        completion(.success(()))
    }

}

struct LoginResource {

    func authenticateUser(withReqeust reqeust: LoginReqeust, completion: @escaping ResultHandler<LoginResponse>) {
        let parameter = reqeust.encodedData

        let webResource = WebResource<LoginResponse>(urlPath: .login, httpMethod: .post(parameter), header: nil, decode: LoginResponse.decode)

        webResource.request { result in
            completion(result)
        }
    }
}

struct UserResource {

    func fetchRequiuser(with completion: @escaping ResultHandler<[RequiUser]>) {
        let webResource = WebResource<RequeUser>(urlPath: .users, httpMethod: .get, header: nil, decode: RequeUser.decode)

        webResource.request { result in
            switch result {
            case .success(let user):
                if let values = user.data, !values.isEmpty {
                    completion(.success(values))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }


    func fetchUser(with completion: @escaping ResultHandler<[User]>) {
        let webResource = WebResource<[User]>(urlPath: .users, httpMethod: .get, header: nil, decode: User.decode)

        webResource.request { result in
            switch result {
            case .success(let response) :
                completion(.success(response))

            case .failure(let error):
                print(error)
            }
        }
    }

    func fetchPost(completion: @escaping ResultHandler<[Post]>)  {
        let webResource = WebResource<[Post]>(urlPath: .posts, httpMethod: .get, header: nil, decode: Post.decode)

        webResource.request { result in
            switch result {
            case .success(let posts):
                completion(.success(posts))
            case .failure(let error):
                print(error)
            }
        }
    }
}

