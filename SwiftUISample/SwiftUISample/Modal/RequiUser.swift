//
//  User.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import Foundation

struct RequeUser: Codable {
    var page: Int?
    var perPage: Int?
    var total: Int?
    var totalPages: Int?
    var data: [RequiUser]?
    var support: Support?

    enum CodingKeys: String, CodingKey {
        case page
        case perPage = "per_page"
        case total
        case totalPages = "total_pages"
        case data, support
    }
}

struct RequiUser: Codable {
    var id: Int?
    var email: String?
    var firstName: String?
    var lastName: String?
    var avatar: String?

    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case avatar
    }
}

extension RequiUser {
    var fullName: String {
        if let fn = self.firstName, let ln = self.lastName {
            return fn + " " + ln
        }
        return ""
    }

    var userEmail: String {
        if let email = self.email {
            return email
        }
        return ""
    }

    var imageAvtar: String {
        if let str = self.avatar {
            return str
        }
        return ""
    }

    var imageData: Data? {
        return imageAvtar.imageData
    }
}

struct Support: Codable {
    var url: String?
    var text: String?
}
