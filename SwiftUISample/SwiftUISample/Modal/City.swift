//
//  City.swift
//  SwiftUISample
//
//  Created by mac-00017 on 02/07/21.
//

import Foundation

// MARK: - City
struct City: Codable, Identifiable {
    var cityId: Int
    var name: String
    var landmarks: [Landmark]

    enum CodingKeys: String, CodingKey {
        case cityId = "cityId"
        case name, landmarks
    }

    var id: Int {
        return self.cityId
    }
}

// MARK: - Landmark
struct Landmark: Codable, Identifiable {
    var landmarkId: Int
    var name: String
    var photo: String
    var landmarkDescription: String

    enum CodingKeys: String, CodingKey {
        case landmarkId = "landmarkId"
        case name, photo
        case landmarkDescription = "description"
    }

    var id: Int {
        return self.landmarkId
    }
}

extension Landmark {

    static var landmark: Landmark {
        return Landmark(landmarkId: 1, name: "HowrahBridge", photo: "test", landmarkDescription: "test")
    }
}
