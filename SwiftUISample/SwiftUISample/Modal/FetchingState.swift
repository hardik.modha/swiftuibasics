//
//  FetchingState.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import Foundation

enum FetchingState {
    case none
    case fetching
    case fetched

    var isFetching: Bool {
        return self == .fetching
    }
}
