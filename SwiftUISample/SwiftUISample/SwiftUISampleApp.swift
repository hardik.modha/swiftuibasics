//
//  SwiftUISampleApp.swift
//  SwiftUISample
//
//  Created by mac-00017 on 01/07/21.
//

import SwiftUI

@main
struct SwiftUISampleApp: App {
    var body: some Scene {
        WindowGroup {
           TaskListView()
        }
    }
}
