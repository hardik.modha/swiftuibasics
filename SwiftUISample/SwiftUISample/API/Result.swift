//
//  Result.swift
//  PaginationSample
//
//  Created by mac-00021 on 14/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import Foundation

enum Result<A> {
    case success(A)
    case failure(AppError)
    
    var value: A? {
        switch self {
        case .success(let val):
            return val
        case .failure:
            return nil
        }
    }

    var error: AppError? {
        switch self {
        case .failure(let error):
            return error
        default:
            return nil
        }
    }
}
