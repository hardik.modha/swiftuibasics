//
//  AppError.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import Foundation

enum AppError {
    case noData
    case custom(APIError)
}

extension AppError {

    var title: String {
        switch self {
        case .noData:
            return "No Data"
        case .custom(let error):
            return error.title
        }
    }

    var message: String {
        switch self {
        case .noData:
            return "No records available"
        case .custom(let error):
            return error.message
        }
    }
}
