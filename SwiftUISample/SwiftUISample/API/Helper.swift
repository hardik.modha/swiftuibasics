//
//  Helper.swift
//  PaginationSample
//
//  Created by mac-00021 on 14/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import Foundation

typealias JSONType = [String: Any]
typealias Header = [String: String]
typealias ResultHandler<A> = (Result<A>) -> Void
typealias EmptyHandler = () -> Void
typealias ConfirmationHandler = (Bool) -> Void
typealias ResponseStatusHandler = (Bool, String) -> Void
