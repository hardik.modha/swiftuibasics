//
//  JOSNHelper.swift
//  SwiftUISample
//
//  Created by mac-00017 on 02/07/21.
//

import Foundation

struct JSONHelper {

    static func getCities() -> [City] {
        do {
            if let fileURL = Bundle.main.url(forResource: "City", withExtension: "json") {
                let data = try Data(contentsOf: fileURL)
                let response = try JSONDecoder().decode([City].self, from: data)
                return response
            }
        } catch let error  {
            print(error.localizedDescription)
            return []
        }
        return []

    }

}
