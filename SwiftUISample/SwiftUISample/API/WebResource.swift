//
//  WebResource.swift
//  PaginationSample
//
//  Created by mac-00021 on 14/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import Foundation
import Alamofire

enum HTTPMethod {
    case get
    case post(JSONType?)
    case put(JSONType?)
    
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
       }
    }
    
    var parameter: JSONType? {
        switch self {
        case .get:
            return nil
        case .post(let param), .put(let param):
            return param
        }
    }
}

struct WebResource<A> {
    var urlPath: URLPath
    var httpMethod: HTTPMethod
    var header: Header?
    var decode: (Data) -> Result<A>
    
    func request(completion: @escaping ResultHandler<A>) {
        WebServiceManager.shared.fetch(resource: self, completion: completion)
    }
}
