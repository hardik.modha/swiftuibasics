//
//  URLPath.swift
//  PaginationSample
//
//  Created by mac-00021 on 14/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import Foundation
import Moya
//https://reqres.in/
//https://jsonplaceholder.typicode.com
enum URLScheme: String {
    case http
    case https
}

enum URLHost {
    case reqres
    case jsonPlaceholder
    
    var baseURL: String {
        switch self {
        case .reqres:
            return "reqres.in"
        case .jsonPlaceholder:
            return "jsonplaceholder.typicode.com"
        }
    }
    
    var scheme: URLScheme {
        return .https
    }
    
    var fixedPath: String {
        switch self {
        case .reqres:
            return "/api/"
        case .jsonPlaceholder:
            return "/"
        }
    }
}

enum URLPath {
    case login
    case users
    case posts
    
    
    
    var endPoint: String {
        switch self {
        case .login:
            return "login"
        case .users:
            return "users"
        case .posts:
            return "posts"
        }
    }
    
    var queryItem: [URLQueryItem]? {
        return nil
    }
    
    
    var url: URL? {
        var urlComponets = URLComponents()
        urlComponets.scheme = AppConfig.host.scheme.rawValue
        urlComponets.host = AppConfig.host.baseURL
        urlComponets.queryItems = self.queryItem
        urlComponets.path = AppConfig.host.fixedPath + self.endPoint
        return urlComponets.url
    }
}


extension URLPath: TargetType {
    var baseURL: URL {
        switch AppConfig.host {
        case .reqres:
            return URL(string: "https://reqres.in")!
        case .jsonPlaceholder:
            return URL(string: "https://jsonplaceholder.typicode.com")!
        }
    }

    var path: String {
        return AppConfig.host.fixedPath + self.endPoint
    }

    var method: Moya.Method {
        switch self {
        case .login, .posts:
            return .post

        case .users:
            return .get

        }
    }



    var sampleData: Data {
        return Data()
    }

    var task: Moya.Task {
        switch self {
        case .login:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        case .users, .posts:
            return .requestPlain
        }
    }

    var headers: [String : String]? {
        return nil
    }


}
