//
//  WebServiceManager.swift
//  PaginationSample
//
//  Created by mac-00021 on 14/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import Foundation
import Alamofire

class WebServiceManager {
    
    private init() {}
    static let shared = WebServiceManager()
    
    func fetch<A>(resource: WebResource<A>, completion: @escaping ResultHandler<A>) {
        
        guard let url = resource.urlPath.url else {
            fatalError("Provide valid url")
        }
        
        print("URL: \(url)")
        var headers: HTTPHeaders?
        let method = resource.httpMethod.method
        let parameter = resource.httpMethod.parameter
        if let header = resource.header {
            headers = HTTPHeaders(header)
        }
        
        AF.request(url, method: method, parameters: parameter, encoding: URLEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseData { (response) in
            
            if let httpResponse = response.response {
                
                switch httpResponse.statusCode {
                case 401:
                    // Show session expired alert here...
                    print("Session Expired!")
                case 200:
                    switch response.result {
                    case .success(let data):
                        completion(resource.decode(data))
                    case .failure(let error):
                        debugPrint(error)
                    }
                default:
                    break
                }
            }
        }
    }
    
    
}
