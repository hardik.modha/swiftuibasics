//
//  LoginViewModel.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import Foundation

class LoginViewModel: ObservableObject {
    @Published var email: String = "eve.holt@reqres.in"
    @Published var password: String = "cityslicka"
    @Published var error: AppError = .noData
    @Published var isShowAlert: Bool = false
    @Published var isNavigate: Bool = false
    var isValid: Bool = false

    private let loginValidation = LoginValidation()
    private let loginResource = LoginResource()

    func validateEntries() {
        loginValidation.validateInputs(withEmail: email, andPassword: password) { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case .success(_):
                strongSelf.isValid = true
            case .failure(let error):
                strongSelf.isValid = false
                strongSelf.error = error
            }
            strongSelf.isShowAlert = !strongSelf.isValid
        }
    }

    func authenticatedUser() {
        let loginReqeust = LoginReqeust(email: self.email, password: self.password)

        loginResource.authenticateUser(withReqeust: loginReqeust) { result in
            switch result {
            case .success(let response):
                print(response)
                self.isNavigate = true
            case .failure(let error):
                self.isShowAlert = true
                self.error = error
            }
        }

    }
}
