//
//  UserViewModal.swift
//  SwiftUISample
//
//  Created by mac-00017 on 05/07/21.
//

import Foundation

class UserViewModal: ObservableObject {
    @Published var user: [User] = [User]()
    @Published var requiuser: [RequiUser] = [RequiUser]()
    @Published var posts: [Post] = [Post]()
    private let userResource = UserResource()



    func fetchRequiuser(with completion: @escaping ConfirmationHandler) {
        userResource.fetchRequiuser { result in
            switch result {
            case .success(let users):
                self.requiuser = users
                completion(true)
            case .failure(let error):
                print(error)
                completion(false)

            }
        }
    }

    func fetchUser(with completion: @escaping ConfirmationHandler) {
        userResource.fetchUser { result in
            switch result {
            case .success(let users):
                self.user = users
                completion(true)
            case .failure(let error):
                completion(false)
                print(error)
            }
        }
    }

    func fetchPost(with completion: @escaping ConfirmationHandler)  {
        userResource.fetchPost() { result in
            switch result {
            case .success(let posts):
                self.posts = posts
                completion(true)
            case .failure(let error):
                completion(false)
                print(error)
            }
        }
        
    }

}
