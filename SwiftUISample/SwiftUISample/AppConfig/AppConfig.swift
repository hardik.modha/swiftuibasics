//
//  AppConfig.swift
//  PaginationSample
//
//  Created by mac-00021 on 14/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import Foundation

struct AppConfig {
    static let host: URLHost = .jsonPlaceholder
}
